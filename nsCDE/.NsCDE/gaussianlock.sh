#!/bin/bash

# Dependencies:
# i3lock
# scrot
# imagemagick
# xautolock

file="/tmp/screenshot.png"
if [ -f "$file" ]
then
    rm $file
fi
scrot /tmp/screenshot.png
convert /tmp/screenshot.png -blur 0x5 /tmp/screenshotblur.png
i3lock -i /tmp/screenshotblur.png

